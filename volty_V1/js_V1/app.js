login = {
    michel: "816821069556-6hv90l70gcb1krd4k5vg328957m8hdlp.apps.googleusercontent.com",
    fifou: "448994739387-q1d7a3mlqijck69dd5r44rncvlilaai0.apps.googleusercontent.com",
    martin: "291213539392-aac6982cq8230fo78thi842i8ttd6kmt.apps.googleusercontent.com"
}
api = {
    michel: 'AIzaSyAWHk-1mPS9z__bckoYa7tdKsJyCbwMzfs',
    fifou: 'AIzaSyAbgrNR5lrJBK0deG1aZ7NZnx5_EjAxKx4',
    martin: 'AIzaSyAPepe9glTapIOLx5ZKE0Wc7Aq0-cRI7oo'
}



var CLIENT_ID = "816821069556-6hv90l70gcb1krd4k5vg328957m8hdlp.apps.googleusercontent.com";
var API_KEY = 'AIzaSyAWHk-1mPS9z__bckoYa7tdKsJyCbwMzfs';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

var authorizeButton = document.getElementById('authorize_button');
var signoutButton = document.getElementById('signout_button');

function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}

function initClient() {
    gapi.client.init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
    }).then(function() {
        // Listen for sign-in state changes.
        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

        // Handle the initial sign-in state.
        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        authorizeButton.onclick = handleAuthClick;
        signoutButton.onclick = handleSignoutClick;
    }, function(error) {
        $("#content").append(JSON.stringify(error, null, 2));
    });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        authorizeButton.style.display = 'none';
        signoutButton.style.display = 'block';
        listUpcomingEvents();
    } else {
        authorizeButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
    $("a,div,h,p,button").remove(".adios")
}


function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
    $("a,div,h,p,button").remove(".adios")
    $(".affichage").prepend(`<p class="adios">Veuillez selectionner votre compte :</p>`)
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
// function appendPre(message) {
//     var pre = document.getElementById('content');
//     var textContent = document.createTextNode(message + '\n');
//     pre.appendChild(textContent);
// }

function listUpcomingEvents() {
    gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': (new Date()).toISOString(),
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 10,
        'orderBy': 'startTime'
    }).then(function(response) {

        afficherTexte(response.result.items);

    });
}



function afficherTexte(data) {





    for (var i = 0; i < data.length; i++) {

        $(".affichage").append(`
            <div class="liste adios" id="${data[i].id}">
                <h class="adios">${data[i].summary}</h>
                <p class="adios">${data[i].location}</p>
                <p class="adios">&nbspde ${data[i].start.dateTime.slice(11, 16)} a ${data[i].end.dateTime.slice(11, 16)}</p>
            </div>`)

    }

    $(".liste").on("click", function() {
        let id = $(this).attr("id")
        $("a,div,h,p,button").remove(".adios")
        for (var i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                $(".affichage").append(`
                <div class="listeBtn">
                    <div class="arrive adios">
                        <p1>Je viens d'arriver</p1>
                    </div>
                    <div class="listeRetour adios" >
                        <div class="mission">
                            <h class="adios">${data[i].summary}</h>
                            <p class="adios">${data[i].location}</p>
                            <p class="adios">de ${data[i].start.dateTime.slice(11, 16)} a ${data[i].end.dateTime.slice(11, 16)}</p>
                        </div>
                        <div class="retour">
                            <p1>Retour</p1>
                        </div>
                    </div> 
                    <div class="part adios">
                        <p1>Je pars</p1>
                    </div>
                </div>`)
            }
        }
        $(".arrive").on("click", function() {
            console.log(id)
            $("a,div,h,p,button").remove(".adios")
            let url = "https://hooks.slack.com/services/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";
            let payload = { "text": "Je suis arrivé." };

            $.post(url, JSON.stringify(payload), (data) => {
                $().text(data);
            })
            alert("message envoyé")
            afficherTexte(data)

        })
        $(".part").on("click", function() {
            $("a,div,h,p,button").remove(".adios")
            let url = "https://hooks.slack.com/services/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";
            let payload = {
                "text": "Je pars."
            };

            $.post(url, JSON.stringify(payload), (data) => {
                $().text(data);
            })
            alert("message envoyé")
            afficherTexte(data)
        })

        $(".retour").on("click", function() {
            $("a,div,h,p,button").remove(".adios")
            afficherTexte(data)
        })


    })
}