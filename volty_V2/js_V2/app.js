// Insérer dans 'urlSlack' les emails de vos plombiers et l'url du webhook associé
urlSlack = {
    "michel.dupont31400@gmail.com": "https://hooks.slack.com/services/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",
    "martin.lelutin31@gmail.com": "https://hooks.slack.com/services/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",
    "lefoufoufifou04@gmail.com": "https://hooks.slack.com/services/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"
}

// Il est nécessaire de rentrer 1 client ID, pour ce faire cf le Read me 
var CLIENT_ID = "816821069556-6hv90l70gcb1krd4k5vg328957m8hdlp.apps.googleusercontent.com";
var API_KEY = 'AIzaSyAWHk-1mPS9z__bckoYa7tdKsJyCbwMzfs';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

var authorizeButton = document.getElementById('authorize_button');
var signoutButton = document.getElementById('signout_button');

function handleClientLoad() {
    gapi.load('client:auth2', initClient);
}

function initClient() {
    gapi.client.init({
        apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES
    }).then(function() {

        gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);


        updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        authorizeButton.onclick = handleAuthClick;
        signoutButton.onclick = handleSignoutClick;
    }, function(error) {
        $("#content").append(JSON.stringify(error, null, 2));
    });
}

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
        authorizeButton.style.display = 'none';
        signoutButton.style.display = 'block';
        listUpcomingEvents();
    } else {
        authorizeButton.style.display = 'block';
        signoutButton.style.display = 'none';
    }
}

function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
    $("a,div,h,p,button").remove(".delete")
}


function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
    $("a,div,h,p,button").remove(".delete")
    $(".affichage").prepend(`<p class="delete">Veuillez selectionner votre compte :</p>`)
}

// Modification du maxResults pour afficher plus de 10 missions par jour

function listUpcomingEvents() {
    var date_start = (new Date()).toISOString()
    var date = date_start.slice(0, 11)
    gapi.client.calendar.events.list({
        'calendarId': 'primary',
        'timeMin': date + "00:10:00.000Z", // Mise en forme pour l'affichage des missions d'une jounée uniquement 
        'timeMax': date + "23:00:00.000Z",
        'showDeleted': false,
        'singleEvents': true,
        'maxResults': 100,
        'orderBy': 'startTime'
    }).then(function(response) {

        afficherTexte(response.result.items);

    });
}
// Fonction déclenchant l'affichage, la disparition et l'envoi sur slack des éléments 

function afficherTexte(data) {

    Email = gapi.auth2.getAuthInstance().currentUser.Ab.w3.U3;

    for (var i = 0; i < data.length; i++) {

        $(".affichage").append(`
        <div class="liste delete" id="${data[i].id}">
            <h class="delete">${data[i].summary}</h>
            <p class="delete">${data[i].location}</p>
            <p class="delete">de ${data[i].start.dateTime.slice(11, 16)} a ${data[i].end.dateTime.slice(11, 16)}</p>
        </div>`)

    }

    $(".liste").on("click", function() {
        let id = $(this).attr("id")
        $("a,div,h,p,button").remove(".delete")
        for (var i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                $(".affichage").append(`
                <div class="listeBtn">
                    <div class="arrive delete">
                        <p1>Je viens d'arriver</p1>
                    </div>
                    <div class="listeRetour delete" >
                        <div class="retour">
                            <p1>Retour</p1>
                        </div>
                        <div class="mission">
                            <h class="delete">${data[i].summary}</h>
                            <p class="delete">${data[i].location}</p>
                            <p class="delete">de ${data[i].start.dateTime.slice(11, 16)} a ${data[i].end.dateTime.slice(11, 16)}</p>
                        </div>
                        
                    </div> 
                    <div class="part delete">
                        <p1>Je pars</p1>
                    </div>
                </div>`)
            }
        }

        // Vous pouvez personnaliser vos messages à envoyer sur Slack en modifiant le texte de la variable 'payload' et y intégrer des variables
        $(".arrive").on("click", function() {

            $("a,div,h,p,button").remove(".delete")
            let url = urlSlack[Email];
            let payload = { "text": "Je suis arrivé." };

            $.post(url, JSON.stringify(payload), (data) => {
                $().text(data);
            })
            alert("message envoyé")
            afficherTexte(data)

        })
        $(".part").on("click", function() {
            $("a,div,h,p,button").remove(".delete")
            let url = urlSlack[Email];
            let payload = {
                "text": "Je pars."
            };

            $.post(url, JSON.stringify(payload), (data) => {
                $().text(data);
            })
            alert("message envoyé")
            afficherTexte(data)
        })

        $(".retour").on("click", function() {
            $("a,div,h,p,button").remove(".delete")
            afficherTexte(data)
        })


    })
}